import yaml

yaml_data = """
files:
  - name: example_file_1.xlsx
    sql:
      - name: query_1
        query: "SELECT * FROM table_1"
      - name: query_2
        query: "SELECT * FROM table_2"
  - name: example_file_2.xlsx
    sql:
      - name: query_1
        query: "SELECT * FROM table_3"
      - name: query_2
        query: "SELECT * FROM table_4"
  - name: example_file_3.xlsx
    sql:
      - name: query_1
        query: "SELECT * FROM table_5"
      - name: query_2
        query: "SELECT * FROM table_6"
"""

def parse_and_print_yaml(yaml_content):
    parsed_yaml = yaml.safe_load(yaml_content)
    for file in parsed_yaml['files']:
        print(f"Excel File: {file['name']}")
        for sql in file['sql']:
            print(f"  SQL Name: {sql['name']}")
            print(f"  Query: {sql['query']}")

parse_and_print_yaml(yaml_data)
