#!/bin/bash

# Update the system
sudo yum update -y

# Install required dependencies
sudo yum install -y git gcc openssl-devel bzip2-devel libffi-devel zlib-devel xz-devel wget

# Install Node.js using nvm
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.0/install.sh | bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
nvm install --lts

# Install Python 3.11
sudo yum install -y gcc openssl-devel bzip2-devel libffi-devel make

wget -c https://www.python.org/ftp/python/3.11.3/Python-3.11.3.tgz
tar -xzf Python-3.11.3.tgz
cd Python-3.11.3
./configure --enable-optimizations
sudo make altinstall
cd ..
rm -rf Python-3.11.3 Python-3.11.3.tgz

# Clone the Open WebUI repository
git clone https://github.com/open-webui/open-webui.git
cd open-webui/

# Copy the required .env file
cp -RPp .env.example .env

# Build the frontend using Node.js
npm i
npm run build

# Install backend dependencies
cd ./backend
pip3.11 install -r requirements.txt -U

# Start the backend server
bash start.sh