CREATE PROCEDURE GetTableRowCounts
AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @TableName NVARCHAR(256), @DynamicSQL NVARCHAR(MAX);

    -- Table variable to store the row counts
    DECLARE @TableCounts TABLE (TableName NVARCHAR(256), RowCount INT);

    -- Cursor to iterate through all user tables
    DECLARE TableCursor CURSOR FOR 
    SELECT QUOTENAME(TABLE_SCHEMA) + '.' + QUOTENAME(TABLE_NAME)
    FROM INFORMATION_SCHEMA.TABLES 
    WHERE TABLE_TYPE = 'BASE TABLE';

    OPEN TableCursor;

    -- Fetch the first table name
    FETCH NEXT FROM TableCursor INTO @TableName;

    -- Iterate through all tables
    WHILE @@FETCH_STATUS = 0
    BEGIN
        -- Dynamic SQL to get the row count of the current table
        SET @DynamicSQL = N'SELECT @RowCountResult = COUNT(*) FROM ' + @TableName;
        
        -- Declare a variable to store the row count
        DECLARE @RowCountResult INT;
        
        -- Execute the dynamic SQL
        EXEC sp_executesql @DynamicSQL, N'@RowCountResult INT OUTPUT', @RowCountResult OUTPUT;
        
        -- Insert the table name and row count into the table variable
        INSERT INTO @TableCounts (TableName, RowCount) VALUES (@TableName, @RowCountResult);
        
        -- Move to the next table
        FETCH NEXT FROM TableCursor INTO @TableName;
    END

    CLOSE TableCursor;
    DEALLOCATE TableCursor;

    -- Select the final results
    SELECT * FROM @TableCounts ORDER BY RowCount DESC, TableName;

    SET NOCOUNT OFF;
END
GO
