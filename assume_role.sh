# Assume the role and capture the output
output=$(aws sts assume-role --role-arn arn:aws:iam::123456789012:role/example-role --role-session-name my-session)

# Extract credentials and set them as environment variables
export AWS_ACCESS_KEY_ID=$(echo $output | jq -r .Credentials.AccessKeyId)
export AWS_SECRET_ACCESS_KEY=$(echo $output | jq -r .Credentials.SecretAccessKey)
export AWS_SESSION_TOKEN=$(echo $output | jq -r .Credentials.SessionToken)

# Python script to write environment variables to a .env file

# Define your environment variables as a dictionary
env_vars = {
    'DATABASE_URL': 'mysql://user:password@localhost/mydatabase',
    'SECRET_KEY': 'your_secret_key_here',
    'DEBUG': 'True'  # Example of a boolean value, note it's as a string
}

# Path to the .env file
env_file_path = 'path/to/your/.env'

# Write the environment variables to the .env file
with open(env_file_path, 'w') as f:
    for key, value in env_vars.items():
        f.write(f'{key}="{value}"\n')
