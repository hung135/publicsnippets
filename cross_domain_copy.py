import boto3
from botocore.exceptions import ClientError

def list_files_from_cross_domain_s3_bucket(aws_access_key_id, aws_secret_access_key, role_arn_xxx, role_arn_yyy, s3_bucket_name):
    """
    List files in an S3 bucket accessible by a cross-domain role "YYY", assuming the initial role "XXX" with my AWS credentials.

    Parameters:
    - aws_access_key_id: My AWS access key ID.
    - aws_secret_access_key: My AWS secret access key.
    - role_arn_xxx: The ARN of role "XXX" which I can assume directly with my credentials.
    - role_arn_yyy: The ARN of cross-domain role "YYY" which is accessible via role "XXX".
    - s3_bucket_name: The name of the S3 bucket to list files from, accessible by role "YYY".
    """

    # Initialize the AWS STS client with my credentials
    sts_client = boto3.client(
        'sts',
        aws_access_key_id=aws_access_key_id,
        aws_secret_access_key=aws_secret_access_key
    )

    # Assume role "XXX" using my credentials
    assumed_role_xxx = sts_client.assume_role(
        RoleArn=role_arn_xxx,
        RoleSessionName="AssumeRoleSessionXXX"
    )

    # Extract the temporary security credentials from assuming role "XXX"
    credentials_xxx = assumed_role_xxx['Credentials']

    # Assume role "YYY" using the credentials of role "XXX"
    assumed_role_yyy = sts_client.assume_role(
        RoleArn=role_arn_yyy,
        RoleSessionName="AssumeRoleSessionYYY",
        Credentials=credentials_xxx
    )

    # Extract the temporary security credentials from assuming role "YYY"
    credentials_yyy = assumed_role_yyy['Credentials']

    # Initialize the S3 client with the credentials of role "YYY" (assumed via role "XXX")
    s3 = boto3.client(
        's3',
        aws_access_key_id=credentials_yyy['AccessKeyId'],
        aws_secret_access_key=credentials_yyy['SecretAccessKey'],
        aws_session_token=credentials_yyy['SessionToken']
    )

    # List all objects in the S3 bucket accessible by role "YYY" (using role "XXX"'s permissions)
    object_keys = []
    paginator = s3.get_paginator('list_objects')
    for page in paginator.paginate(Bucket=s3_bucket_name):
        try:
            object_keys += [obj['Key'] for obj in page['Contents']]
        except ClientError as e:
            print(e)
            raise

    return object_keys