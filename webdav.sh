#!/bin/bash

# Check if davfs2 is installed
if ! command -v mount.davfs > /dev/null 2>&1; then
    echo "Installing davfs2..."
    sudo apt-get update
    sudo apt-get install -y davfs2
fi

# Set the WebDAV URL, username, and password
webdav_url="<WebDAV_URL>"
username="<username>"
password="<password>"

# Set the mount point directory
mount_point="/mnt/webdav"

# Create the mount point directory if it doesn't exist
sudo mkdir -p "$mount_point"

# Add the WebDAV URL to /etc/fstab for automatic mounting
echo "$webdav_url $mount_point davfs user,noauto,_netdev 0 0" | sudo tee -a /etc/fstab > /dev/null

# Configure davfs2
sudo sed -i 's/^# use_locks 0$/use_locks 0/' /etc/davfs2/davfs2.conf

# Create the secrets file with WebDAV credentials
echo "$webdav_url $username $password" | sudo tee /etc/davfs2/secrets > /dev/null
sudo chmod 600 /etc/davfs2/secrets

# Mount the WebDAV filesystem
sudo mount -t davfs "$webdav_url" "$mount_point"

echo "WebDAV filesystem mounted at $mount_point"