import os
import hashlib

def season_environment_variable(variable_name, seed):
    """
    Seasons an environment variable using a seed.

    Args:
    - variable_name: The name of the environment variable to season.
    - seed: The seed (secret key) used for seasoning.

    Returns:
    - The seasoned value of the environment variable.
    """
    # Get the original value of the environment variable
    original_value = os.getenv(variable_name)

    if original_value is None:
        raise ValueError(f"Environment variable '{variable_name}' does not exist.")

    # Combine the original value and the seed
    seasoned_value = original_value + seed

    # Hash the combined value
    hashed_value = hashlib.sha256(seasoned_value.encode()).hexdigest()

    return hashed_value

def unseason_environment_variable(seasoned_value, seed):
    """
    Unseasons a seasoned environment variable using the original seed.

    Args:
    - seasoned_value: The seasoned value of the environment variable.
    - seed: The seed (secret key) used for seasoning.

    Returns:
    - The original value of the environment variable.
    """
    # Remove the seed from the seasoned value
    original_value = seasoned_value.replace(seed, '')

    return original_value
