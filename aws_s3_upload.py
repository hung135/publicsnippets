import urllib.request
import datetime
import hashlib
import hmac
import base64

def sign(key, msg):
    return hmac.new(key, msg.encode('utf-8'), hashlib.sha256).digest()

def getSignatureKey(key, dateStamp, regionName, serviceName):
    kDate = sign(('AWS4' + key).encode('utf-8'), dateStamp)
    kRegion = sign(kDate, regionName)
    kService = sign(kRegion, serviceName)
    kSigning = sign(kService, 'aws4_request')
    return kSigning

def upload_to_s3(file_path, bucket, object_name, aws_access_key, aws_secret_key, region):
    method = 'PUT'
    service = 's3'
    host = f'{bucket}.s3.amazonaws.com'
    endpoint = f'https://{host}/{object_name}'
    
    t = datetime.datetime.utcnow()
    amz_date = t.strftime('%Y%m%dT%H%M%SZ')
    date_stamp = t.strftime('%Y%m%d')

    policy = base64.b64encode(json.dumps({
        "expiration": (t + datetime.timedelta(hours=1)).strftime('%Y-%m-%dT%H:%M:%SZ'),
        "conditions": [
            {"bucket": bucket},
            {"key": object_name},
            {"acl": "private"},
            ["content-length-range", 0, 1048576],
            {"x-amz-algorithm": "AWS4-HMAC-SHA256"},
            {"x-amz-credential": f"{aws_access_key}/{date_stamp}/{region}/{service}/aws4_request"},
            {"x-amz-date": amz_date}
        ]
    }).encode('utf-8')).decode('utf-8')

    signature = base64.b64encode(sign(
        getSignatureKey(aws_secret_key, date_stamp, region, service),
        policy
    )).decode('utf-8')

    with open(file_path, 'rb') as f:
        data = f.read()

    req = urllib.request.Request(endpoint, data=data, method=method)
    req.add_header('Host', host)
    req.add_header('Date', amz_date)
    req.add_header('X-Amz-Date', amz_date)
    req.add_header('X-Amz-Content-Sha256', hashlib.sha256(data).hexdigest())
    req.add_header('Authorization', f'AWS4-HMAC-SHA256 Credential={aws_access_key}/{date_stamp}/{region}/{service}/aws4_request, SignedHeaders=host;x-amz-content-sha256;x-amz-date, Signature={signature}')
    
    try:
        response = urllib.request.urlopen(req)
        print(f"Upload Successful. Status code: {response.getcode()}")
    except urllib.error.HTTPError as e:
        print(f"Upload Failed. Error code: {e.code}\nError message: {e.read().decode('utf-8')}")

# Usage
upload_to_s3(
    'path/to/your/file.txt',
    'your-bucket-name',
    'desired-object-name-in-s3.txt',
    'YOUR_AWS_ACCESS_KEY',
    'YOUR_AWS_SECRET_KEY',
    'your-region'  # e.g., 'us-east-1'
)