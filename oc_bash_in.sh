#!/bin/bash

# Variables
NAMESPACE="some-deploy-db-dev"  # Your namespace
APP_LABEL="app=xxx-postgresql"  # Label selector for the pod
SQL_FOLDER_PATH="./sql"  # Path to your SQL scripts folder
CONTAINER_NAME="<ContainerName>"  # Container name, if needed

# Get a list of all matching pods
PODS=$(oc get pods -n "$NAMESPACE" -l "$APP_LABEL" -o jsonpath="{.items[*].metadata.name}")

# Check if no pods were found
if [ -z "$PODS" ]; then
    echo "No pods found with label $APP_LABEL in namespace $NAMESPACE."
    exit 1
fi

# Loop through each pod
for POD_NAME in $PODS; do
    echo "Found pod: $POD_NAME"

    # Loop over all SQL scripts in the SQL_FOLDER_PATH
    for SQL_SCRIPT_PATH in $SQL_FOLDER_PATH/*; do
        SCRIPT_NAME=$(basename "$SQL_SCRIPT_PATH")

        echo "Copying SQL script $SCRIPT_NAME to pod: $POD_NAME"

        # Copy the SQL script to the PostgreSQL container
        oc cp "$SQL_SCRIPT_PATH" "$NAMESPACE/$POD_NAME:/tmp/$SCRIPT_NAME" --container="$CONTAINER_NAME"

        echo "Executing SQL script $SCRIPT_NAME on pod: $POD_NAME"
        # Execute the SQL script inside the PostgreSQL container
        oc exec -n "$NAMESPACE" "$POD_NAME" --container="$CONTAINER_NAME" -- bash -c "psql -U <database_user> -d <database_name> -f /tmp/$SCRIPT_NAME"

        echo "SQL script $SCRIPT_NAME executed successfully on pod: $POD_NAME"
    done
done
