# README for XXX-PostgreSQL Template

This document describes the OpenShift template defined in the YAML file for deploying a PostgreSQL database instance. The template is designed to be flexible and easily adaptable for different applications by adjusting parameters. However, this README focuses on explaining the core components (objects) and key labels within the YAML file.

## Overview

The template includes several Kubernetes/OpenShift objects necessary for deploying a PostgreSQL database. These objects include a PersistentVolumeClaim for database storage, a Deployment for the PostgreSQL server, a Service for network access, and a Route for external access.

### PersistentVolumeClaim (PVC)

- **Purpose**: Allocates storage for the PostgreSQL database data. This ensures that the data persists across pod restarts and deployments.
- **Important Fields**:
  - `accessModes`: Specifies the access mode as `ReadWriteOnce`, meaning the volume can be mounted as read-write by a single node.
  - `requests`: Defines the size of the storage requested for the PostgreSQL data, customizable via the `VOLUME_CAPACITY` parameter.

```yaml
- apiVersion: v1
  kind: PersistentVolumeClaim
  metadata:
    name: "${APPLICATION_NAME}-postgresql-pvc"
    namespace: ${NAMESPACE}
  spec:
    accessModes:
      - ReadWriteOnce
    resources:
      requests:
        storage: ${VOLUME_CAPACITY}
```

### Deployment

- **Purpose**: Manages the deployment and scaling of a specified number of PostgreSQL server instances.
- **Key Labels**:
  - `app`: Uses the `${APPLICATION_NAME}-postgresql` value.
- **Important Fields**:
  - `replicas`: Number of pods to run.
  - `containers`:
    - `image`: Docker image for the PostgreSQL server.
    - `ports`: Container port for PostgreSQL.
    - `env`: Environment variables for the PostgreSQL configuration.
    - `volumeMounts`: Mounts the PVC to the container.
    - `resources`: CPU and memory resources.
    - `securityContext`: Security settings.

```yaml
- apiVersion: apps/v1
  kind: Deployment
  metadata:
    name: "${APPLICATION_NAME}-postgresql"
    namespace: ${NAMESPACE}
    labels:
      app: "${APPLICATION_NAME}-postgresql"
  spec:
    replicas: 1
    selector:
      matchLabels:
        app: "${APPLICATION_NAME}-postgresql"
    template:
      metadata:
        labels:
          app: "${APPLICATION_NAME}-postgresql"
      spec:
        containers:
          - name: postgresql
            image: "${REGISTRY_URL}/rhel9/postgresql-${POSTGRESQL_VERSION}:${IMAGE_TAG}"
            ports:
              - containerPort: 5432
```

### Service

- **Purpose**: Exposes the PostgreSQL deployment within the internal OpenShift network.
- **Key Labels**:
  - `app`: Matches the `${APPLICATION_NAME}-postgresql` label.

```yaml
- apiVersion: v1
  kind: Service
  metadata:
    name: "${APPLICATION_NAME}-postgresql"
    namespace: ${NAMESPACE}
  spec:
    ports:
    - port: 5432
    selector:
      app: "${APPLICATION_NAME}-postgresql"
    type: ClusterIP
```

### Route

- **Purpose**: Exposes the PostgreSQL Service outside of the OpenShift cluster.
- **Important Fields**:
  - `to`: Target Service for the Route.
  - `tls`: Configures TLS for secure connections.

```yaml
- apiVersion: route.openshift.io/v1
  kind: Route
  metadata:
    name: "${APPLICATION_NAME}-postgresql-route"
    namespace: ${NAMESPACE}
  spec:
    to:
      kind: Service
      name: "${APPLICATION_NAME}-postgresql"
    port:
      targetPort: 5432
    tls:
      termination: edge
      insecureEdgeTerminationPolicy: Redirect
```

## Summary

This template automates the deployment of a PostgreSQL database on OpenShift, ensuring secure, scalable, and persistent storage. By leveraging Deployment, Service, and Route objects, it facilitates both internal and external connectivity to the database, tailored through easily adjustable parameters.