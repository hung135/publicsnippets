#!/bin/bash

# Set the registry URL
REGISTRY_URL="registry.ollama.ai"

# Set the output file name
OUTPUT_FILE="ollama_ca.pem"

# Retrieve the CA certificate
echo -n | openssl s_client -connect "$REGISTRY_URL:443" -servername "$REGISTRY_URL" 2>/dev/null | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > "$OUTPUT_FILE"

# Check if the certificate file was created successfully
if [ -s "$OUTPUT_FILE" ]; then
  echo "CA certificate retrieved successfully: $OUTPUT_FILE"
else
  echo "Failed to retrieve the CA certificate. Please check the connection and SSL/TLS configuration."
  exit 1
fi

# Display the certificate details
echo "Certificate details:"
cat "$OUTPUT_FILE"

# Copy the certificate to the trust store directory
sudo cp "$OUTPUT_FILE" "/etc/pki/ca-trust/source/anchors/"

# Update the system's CA trust store
sudo update-ca-trust

echo "CA certificate added to the trust store successfully."