#!/bin/bash

# Update your packages
sudo yum update -y

# Install Docker
echo "Installing Docker..."
sudo amazon-linux-extras install docker -y
sudo service docker start
sudo systemctl enable docker
sudo usermod -a -G docker ec2-user

# Install Git
echo "Installing Git..."
sudo yum install git -y

# Install GitLab Runner
echo "Installing GitLab Runner..."
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh | sudo bash
sudo yum install gitlab-runner -y

# Enable and start GitLab Runner
sudo systemctl enable gitlab-runner
sudo systemctl start gitlab-runner

echo "Installation complete."
echo "Please configure GitLab Runner by running 'sudo gitlab-runner register' and follow the prompts."

