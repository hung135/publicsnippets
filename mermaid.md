graph TD
    A[Browser] -->|HTTP/HTTPS| B[NGINX]
    B -->|Proxy| C[Gunicorn]
    C --> D[Django]

    subgraph Backend ["Backend"]
        D --> E[Python Report Code]
        D --> F[Meta Data Code]
        
        subgraph CustomLogic ["Custom Reporting Logic"]
            E
            F
            G[(Postgres)]
            H[Future Direct Query]
        end
        
        F --> G
        E --> H
    end

    H -->|Future| I[(Data Warehouse)]
    D -->|API Calls| J[SAS Viya]
    J -->|Query| I

    K[React App] --> B

    subgraph Build ["Build Process (not runtime)"]
        L[Node.js] -->|Builds| K
    end

    style Backend fill:#f0f0f0,stroke:#333,stroke-width:2px
    style CustomLogic fill:#e6f3ff,stroke:#333,stroke-width:2px
    style Build fill:#e0e0e0,stroke:#333,stroke-width:2px
    style E fill:#90EE90,stroke:#333,stroke-width:2px
    style J fill:#FFB6C1,stroke:#333,stroke-width:2px
    style H fill:#90EE90,stroke:#333,stroke-width:2px