#!/bin/bash

# Update the system
sudo yum update -y

# Install required dependencies
sudo yum install -y kernel-devel-$(uname -r) kernel-headers-$(uname -r) gcc make dkms

# Add the NVIDIA repository
sudo tee /etc/yum.repos.d/nvidia.repo <<EOF
[nvidia]
name=NVIDIA Repository
baseurl=https://developer.download.nvidia.com/compute/cuda/repos/rhel7/x86_64/
enabled=1
gpgcheck=0
EOF

# Install the NVIDIA driver
sudo yum install -y nvidia-driver

# Verify the NVIDIA driver installation
nvidia-smi

# Install Docker
sudo yum install -y docker

# Start and enable Docker service
sudo systemctl start docker
sudo systemctl enable docker

# Add the current user to the docker group
sudo usermod -aG docker $USER

# Install NVIDIA Docker
distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.repo | sudo tee /etc/yum.repos.d/nvidia-docker.repo

sudo yum install -y nvidia-container-toolkit
sudo systemctl restart docker

# Test NVIDIA Docker
docker run --rm --gpus all nvidia/cuda:11.0-base nvidia-smi

# Reboot the system to load the NVIDIA driver
sudo reboot