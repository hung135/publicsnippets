import os
import boto3
from configparser import ConfigParser

def set_env_variables(access_key, secret_key, session_token=None):
    os.environ['AWS_ACCESS_KEY_ID'] = access_key
    os.environ['AWS_SECRET_ACCESS_KEY'] = secret_key
    if session_token:
        os.environ['AWS_SESSION_TOKEN'] = session_token

def get_credentials_from_file():
    credentials_path = os.path.expanduser('~/.aws/credentials')
    config = ConfigParser()
    config.read(credentials_path)
    if 'default' in config.sections():
        return config['default']['aws_access_key_id'], config['default']['aws_secret_access_key']
    else:
        return None, None

def get_credentials_from_secrets_manager(secret_name):
    client = boto3.client('secretsmanager')
    response = client.get_secret_value(SecretId=secret_name)
    secret = eval(response['SecretString'])  # Assuming the secret string is in a dictionary format
    return secret['AWS_ACCESS_KEY_ID'], secret['AWS_SECRET_ACCESS_KEY'], secret.get('AWS_SESSION_TOKEN')

def main():
    access_key, secret_key = get_credentials_from_file()
    if access_key and secret_key:
        print("Credentials found in file.")
    else:
        print("Credentials not found in file. Retrieving from AWS Secrets Manager...")
        # Specify your secret name here
        secret_name = 'your_secret_name_here'
        access_key, secret_key, session_token = get_credentials_from_secrets_manager(secret_name)
    
    set_env_variables(access_key, secret_key, session_token)
    print("Environment variables set.")

if __name__ == '__main__':
    main()
