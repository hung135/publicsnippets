import os
import requests

# Define your SAS Viya API endpoint, token, and folder ID
SAS_VIYA_API_URL = "https://your-sas-viya-api-endpoint.com"
API_TOKEN = "your-api-token"
FOLDER_ID = "your-folder-id"

# Define the directory containing the .sas files
SAS_FILES_DIRECTORY = "/path/to/sas/files/directory"

# Function to upload a SAS report to SAS Viya
def upload_sas_report(file_path):
    # Construct the URL for creating a report in the specified folder
    create_report_url = f"{SAS_VIYA_API_URL}/reports/folders/{FOLDER_ID}/reports"

    # Read the SAS report file
    with open(file_path, 'rb') as sas_file:
        sas_content = sas_file.read()

    # Define the request headers, including the API token
    headers = {
        "Authorization": f"Bearer {API_TOKEN}",
        "Content-Type": "application/octet-stream",
    }

    # Send the HTTP POST request to create the report
    response = requests.post(create_report_url, headers=headers, data=sas_content)

    # Check the response status code
    if response.status_code == 201:
        print(f"Uploaded {file_path} successfully.")
    else:
        print(f"Failed to upload {file_path}. Status code: {response.status_code}")
        print(response.text)

# Iterate through the .sas files in the directory and upload them
for root, dirs, files in os.walk(SAS_FILES_DIRECTORY):
    for file in files:
        if file.endswith(".sas"):
            file_path = os.path.join(root, file)
            upload_sas_report(file_path)
