import requests
import json

# Endpoint URL
url = 'http://localhost:11434/api/chat'

# Payload with the request data
payload = {
    "model": "llama3",
    "messages": [
        {"role": "user", "content": "Hello, how can I help you today?"}
    ]
}

# Headers to specify that the payload is JSON
headers = {'Content-Type': 'application/json'}

# Sending POST request
response = requests.post(url, headers=headers, data=json.dumps(payload))

# Parsing the response
if response.status_code == 200:
    data = response.json()
    # Extracting and printing the content field from the assistant's message
    if 'message' in data and 'content' in data['message']:
        print(data['message']['content'])
    else:
        print("No response content found.")
else:
    print("Failed to get response:", response.status_code)
