#!/bin/bash
# The shebang line tells the system this script should be run with bash

# Database and schema names
DB_NAME="test"  # Sets the name of the database to "test"
SCHEMA_NAME="test"  # Sets the name of the schema to "test"

# Get the list of tables from the specified schema
TABLES=$(psql -d $DB_NAME -t -c "SELECT tablename FROM pg_tables WHERE schemaname = '$SCHEMA_NAME'")
# Executes a PostgreSQL command to list all table names in the specified schema and database,
# and assigns the output to the variable TABLES.

# Loop through each table and dump its schema (DDL) into an individual file
for TABLE in $TABLES; do  # Starts a loop over each table name stored in the TABLES variable
    echo "Dumping schema for $TABLE"  # Echoes a message indicating which table's schema is currently being dumped
    pg_dump -s -d $DB_NAME -t $SCHEMA_NAME.$TABLE > "${TABLE}_schema.sql"
    # Runs pg_dump with the -s (schema-only) option to dump the DDL of the current table.
    # The output is redirected to a file named after the table, with a "_schema.sql" suffix.
done  # Ends the loop

echo "All table schemas dumped."
# Echoes a message indicating that all table schemas have been dumped.
