"""
This script generates:
- A list of all tables in the specified schema
- Columns for each table including name and data type
- Foreign key relationships between tables
The output is a Mermaid ER diagram that represents:
- Tables as entities
- Columns within each table
- Relationships between tables indicating foreign keys
"""

import psycopg2

# Database connection parameters
dbname = 'your_database_name'
user = 'your_username'
password = 'your_password'
host = 'localhost'
schema = 'public'  # or your schema name

# Establish a database connection
conn = psycopg2.connect(dbname=dbname, user=user, password=password, host=host)

# Function to fetch table definitions
def fetch_tables(cursor, schema):
    cursor.execute("""
        SELECT table_name
        FROM information_schema.tables
        WHERE table_schema = %s;
    """, (schema,))
    return [table[0] for table in cursor.fetchall()]

# Function to fetch columns for a table
def fetch_columns(cursor, schema, table_name):
    cursor.execute("""
        SELECT column_name, data_type
        FROM information_schema.columns
        WHERE table_schema = %s AND table_name = %s;
    """, (schema, table_name))
    return cursor.fetchall()

# Function to fetch foreign key relationships
def fetch_relationships(cursor, schema):
    cursor.execute("""
        SELECT tc.table_name, kcu.column_name, ccu.table_name AS foreign_table_name, ccu.column_name AS foreign_column_name 
        FROM 
            information_schema.table_constraints AS tc 
            JOIN information_schema.key_column_usage AS kcu
              ON tc.constraint_name = kcu.constraint_name
              AND tc.table_schema = kcu.table_schema
            JOIN information_schema.constraint_column_usage AS ccu
              ON ccu.constraint_name = tc.constraint_name
              AND ccu.table_schema = tc.table_schema
        WHERE tc.constraint_type = 'FOREIGN KEY' AND tc.table_schema = %s;
    """, (schema,))
    return cursor.fetchall()

# Function to generate Mermaid ER diagram
def generate_mermaid_diagram(tables, table_columns, relationships):
    diagram = "erDiagram\n"
    for table in tables:
        diagram += f"    {table} {{\n"
        for column in table_columns[table]:
            column_name, data_type = column
            # Simplify data type for diagram
            diagram += f"        {data_type} {column_name}\n"
        diagram += "    }\n"
    
    for (table, column, foreign_table, foreign_column) in relationships:
        diagram += f"    {table} ||--|{{ {foreign_table} : \"{column} references {foreign_column}\"\n"

    return diagram

# Main function to fetch database schema and generate diagram
def main():
    with conn.cursor() as cur:
        tables = fetch_tables(cur, schema)
        table_columns = {table: fetch_columns(cur, schema, table) for table in tables}
        relationships = fetch_relationships(cur, schema)
        mermaid_diagram = generate_mermaid_diagram(tables, table_columns, relationships)
        print(mermaid_diagram)

if __name__ == "__main__":
    main()
