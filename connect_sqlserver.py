import sqlalchemy as sa
import os
from dotenv import load_dotenv

# Load environment variables from .env file
load_dotenv()

def get_con():
    # Get database credentials from environment variables
    server_name = os.environ.get("DB_SERVER")
    database_name = os.environ.get("DB_NAME")
    username = os.environ.get("DB_USERNAME")
    password = os.environ.get("DB_PASSWORD")

    if not all([server_name, database_name, username, password]):
        raise Exception("Missing database credentials in .env file")

    # Build the SQL Server connection string
    connection_string = f"mssql+pyodbc://{username}:{password}@{server_name}/{database_name}?driver=ODBC+Driver+17+for+SQL+Server"

    # Create and return the SQLAlchemy connection
    engine = sa.create_engine(connection_string)
    connection = engine.connect()
    return connection

if __name__ == "__main__":
    # Test your connection when running the script directly
    connection = get_con()
    print("Connected to SQL Server")
