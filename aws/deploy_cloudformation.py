import boto3
import json

def deploy_cloudformation():
    # Initialize CloudFormation client
    cf_client = boto3.client('cloudformation')

    # Prompt for stack name, instance name, and description
    stack_name = input("Enter a name for the CloudFormation stack: ")
    instance_name = input("Enter a name for the EC2 instance: ")
    instance_description = input("Enter a description for the EC2 instance: ")

    # Read CloudFormation template
    with open('ec2_template.yaml', 'r') as template_file:
        template_body = template_file.read()

    # Create CloudFormation stack
    response = cf_client.create_stack(
        StackName=stack_name,
        TemplateBody=template_body,
        Parameters=[
            {
                'ParameterKey': 'InstanceName',
                'ParameterValue': instance_name
            },
            {
                'ParameterKey': 'InstanceDescription',
                'ParameterValue': instance_description
            }
        ],
        Capabilities=['CAPABILITY_IAM']
    )

    print(f"Stack creation initiated. Stack ID: {response['StackId']}")

    # Wait for stack creation to complete
    waiter = cf_client.get_waiter('stack_create_complete')
    print("Waiting for stack creation to complete...")
    waiter.wait(StackName=stack_name)

    print("Stack creation completed successfully!")


def list_ec2_instances():
    ec2_client = boto3.client('ec2')
    response = ec2_client.describe_instances()

    for reservation in response['Reservations']:
        for instance in reservation['Instances']:
            # Get the instance name from the 'Name' tag
            instance_name = 'Unnamed'
            for tag in instance.get('Tags', []):
                if tag['Key'] == 'Name':
                    instance_name = tag['Value']
                    break
            
            # Get the launch time and format it
            launch_time = instance['LaunchTime'].strftime('%Y-%m-%d %H:%M:%S')
            
            print(f"Instance Name: {instance_name}, Creation Date: {launch_time}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Deploy a CloudFormation stack from a template or list EC2 instances.')
    parser.add_argument('--template', type=str, default='abc.yaml',
                        help='The filename of the CloudFormation template (default: abc.yaml)')
    parser.add_argument('--list-ec2', action='store_true',
                        help='List all EC2 instances in the account')
    
    args = parser.parse_args()
    
    if args.list_ec2:
        list_ec2_instances()
    else:
        deploy_cloudformation(args.template)