#!/bin/bash

# Exit on any error
set -e

# Configuration
IMAGE_NAME="bedrock-gateway"
TAG="latest"

echo "🚀 Building Bedrock Access Gateway..."

# Create temporary directory
TEMP_DIR=$(mktemp -d)
cd "$TEMP_DIR"

# Clone the repository
echo "📥 Cloning AWS Bedrock Access Gateway repository..."
git clone https://github.com/aws-samples/bedrock-access-gateway.git
cd bedrock-access-gateway/src

# Build the Docker image
echo "🏗️ Building Docker image..."
docker build -f Dockerfile_ecs -t "$IMAGE_NAME:$TAG" .

echo "✅ Done! Image is built as: $IMAGE_NAME:$TAG"
echo "You can now push it to your registry if needed"