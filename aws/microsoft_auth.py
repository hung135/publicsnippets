import requests
from bs4 import BeautifulSoup
import xml.etree.ElementTree as ET
import base64
import os
from urllib.parse import urlparse, parse_qs, urlencode
import urllib3
import warnings
import sys

# Suppress only the single warning from urllib3 needed.
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# Default credentials (DO NOT use real credentials here)
DEFAULT_USERNAME = "default_username"
DEFAULT_PASSWORD = "default_password"

# Override with environment variables if they exist
USERNAME = os.environ.get('USERNAME', DEFAULT_USERNAME)
PASSWORD = os.environ.get('PASSWORD', DEFAULT_PASSWORD)

# SSL Verification flag
VERIFY_SSL = os.environ.get('VERIFY_SSL', 'True').lower() == 'true'

def construct_auth_url(base_url, tenant_id):
    parsed_url = urlparse(base_url)
    query_params = parse_qs(parsed_url.query)
    query_params['tenantid'] = [tenant_id]
    new_query = urlencode(query_params, doseq=True)
    return parsed_url._replace(query=new_query).geturl()

def initiate_saml_auth(auth_url):
    session = requests.Session()
    try:
        response = session.get(auth_url, verify=VERIFY_SSL)
        response.raise_for_status()  # Raises an HTTPError for bad responses
        return session, response
    except requests.exceptions.RequestException as e:
        print(f"Error during authentication request: {e}")
        print(f"Response status code: {e.response.status_code if e.response else 'No response'}")
        print(f"Response content: {e.response.text if e.response else 'No content'}")
        return session, None

def parse_saml_response(response):
    if response is None:
        print("No response to parse")
        return None
    soup = BeautifulSoup(response.text, 'html.parser')
    saml_response = soup.find('input', {'name': 'SAMLResponse'})
    return saml_response['value'] if saml_response else None

def decode_saml_response(encoded_response):
    decoded = base64.b64decode(encoded_response).decode('utf-8')
    root = ET.fromstring(decoded)
    return root

def extract_assertion(root):
    assertion = root.find('{urn:oasis:names:tc:SAML:2.0:assertion}Assertion')
    return ET.tostring(assertion).decode('utf-8') if assertion is not None else None

def main():
    base_url = "https://launcher.myapps.microsoft.com/api/signin/2dasdfasdfsdfssomething"
    tenant_id = "edsadfsdf0343-2342320f"
    
    auth_url = construct_auth_url(base_url, tenant_id)
    print(f"Authentication URL: {auth_url}")
    
    if not VERIFY_SSL:
        warnings.warn("SSL certificate verification is disabled. This is insecure and should only be used in controlled environments.")
    
    session, response = initiate_saml_auth(auth_url)
    if response is None:
        print("Failed to get initial response. Exiting.")
        sys.exit(1)
    
    print(f"Initial response status code: {response.status_code}")
    print(f"Initial response headers: {response.headers}")
    print(f"Initial response content: {response.text[:500]}...")  # Print first 500 characters
    
    login_data = {
        'email': USERNAME,
        'password': PASSWORD,
        'tenantid': tenant_id,
    }
    try:
        response = session.post(auth_url, data=login_data, verify=VERIFY_SSL)
        response.raise_for_status()
    except requests.exceptions.RequestException as e:
        print(f"Error during login POST request: {e}")
        print(f"Response status code: {e.response.status_code if e.response else 'No response'}")
        print(f"Response content: {e.response.text if e.response else 'No content'}")
        sys.exit(1)
    
    print(f"Login response status code: {response.status_code}")
    print(f"Login response headers: {response.headers}")
    print(f"Login response content: {response.text[:500]}...")  # Print first 500 characters
    
    saml_response = parse_saml_response(response)
    if saml_response:
        decoded_response = decode_saml_response(saml_response)
        assertion = extract_assertion(decoded_response)
        
        if assertion:
            print("SAML Assertion obtained successfully.")
            print(f"Assertion: {assertion[:500]}...")  # Print first 500 characters
        else:
            print("Failed to extract SAML Assertion.")
    else:
        print("Failed to obtain SAML Response.")

if __name__ == "__main__":
    main()