import boto3

# Step 2: Initialize Boto3 STS Client
sts_client = boto3.client('sts')

# Step 3: Assume XXX Role
assumed_role_xxx = sts_client.assume_role(
    RoleArn='arn:aws:iam::<account-id>:role/XXX',
    RoleSessionName='AssumeXXXRoleSession'
)
credentials_xxx = assumed_role_xxx['Credentials']

# Step 4: Use Temporary Credentials to Assume YYY Role
sts_client_with_xxx = boto3.client(
    'sts',
    aws_access_key_id=credentials_xxx['AccessKeyId'],
    aws_secret_access_key=credentials_xxx['SecretAccessKey'],
    aws_session_token=credentials_xxx['SessionToken'],
)

assumed_role_yyy = sts_client_with_xxx.assume_role(
    RoleArn='arn:aws:iam::<other-account-id>:role/YYY',
    RoleSessionName='AssumeYYYRoleSession'
)
credentials_yyy = assumed_role_yyy['Credentials']

# Step 5: Access YYY's S3 Bucket
s3_client = boto3.client(
    's3',
    aws_access_key_id=credentials_yyy['AccessKeyId'],
    aws_secret_access_key=credentials_yyy['SecretAccessKey'],
    aws_session_token=credentials_yyy['SessionToken'],
)

# Step 6: List Files in the S3 Bucket
bucket_contents = s3_client.list_objects_v2(Bucket='YYY_s3_bucket')

# Step 7: Process the List of Files
if 'Contents' in bucket_contents:
    for obj in bucket_contents['Contents']:
        print(obj['Key'])
else:
    print("No files found in the bucket.")
