#!/bin/bash

# Define base directory
baseDir="./database"

# Create directories
mkdir -p "$baseDir"/{schemas,tables/views/stored_procedures/functions/triggers/indexes/seeds}/my_schema
mkdir -p ./documentation/{schema_designs,data_models,change_logs}

# Create example table files
touch "$baseDir"/tables/my_schema/my_table.sql

# Create example view files
touch "$baseDir"/views/my_schema/my_view.sql

# Create example stored procedures
touch "$baseDir"/stored_procedures/my_schema/my_procedure.sql

# Create example functions
touch "$baseDir"/functions/my_schema/my_function.sql

# Create example triggers
touch "$baseDir"/triggers/my_schema/my_trigger.sql

# Create example indexes
touch "$baseDir"/indexes/my_schema/my_index.sql

# Create example seed data scripts
touch "$baseDir"/seeds/my_schema/my_seed.sql

# Create documentation and README files
touch ./documentation/schema_designs/my_design.md
touch ./documentation/data_models/my_data_model.md
touch ./documentation/change_logs/CHANGELOG.md
touch README.md

# Create .gitignore
echo "*.log" > .gitignore
echo "temp/" >> .gitignore

echo "Structure created successfully."
