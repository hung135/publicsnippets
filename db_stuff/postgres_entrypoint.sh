#!/bin/bash
set -e

POSTGRES_BIN="/usr/pgsql-15/bin/postgres"
INITDB_BIN="/usr/pgsql-15/bin/initdb"

# Function to initialize the database if it doesn't exist
initialize_db() {
    if [ ! -f "$PGDATA/PG_VERSION" ]; then
        echo "Initializing PostgreSQL database..."
        if [ ! -x "$INITDB_BIN" ]; then
            echo "Error: initdb binary not found or not executable at $INITDB_BIN"
            exit 1
        fi
        su postgres -c "$INITDB_BIN -D $PGDATA"
        echo "host all all 0.0.0.0/0 md5" >> "$PGDATA/pg_hba.conf"
    fi
}

# Initialize the database
initialize_db

if [ ! -x "$POSTGRES_BIN" ]; then
    echo "Error: postgres binary not found or not executable at $POSTGRES_BIN"
    exit 1
fi

echo "Using PostgreSQL binary: $POSTGRES_BIN"

# Start PostgreSQL
exec su postgres -c "$POSTGRES_BIN -D ${PGDATA}"