#!/bin/bash

# Variables
POD_NAME="pod_xyz"
BACKUP_FILE="path/file_name.backup"  # Full path with filename
DB_NAME="designdb"           # Your database name
DB_USER="postgres"           # User to connect with
DB_OWNER="test"              # Owner for the created database

# Extract just the filename from the path
FILENAME=$(basename "$BACKUP_FILE")

# Terminate all connections to the database
echo "Terminating all active sessions to the database..."
oc exec "$POD_NAME" -- bash -c "psql -U $DB_USER -c \"SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname = '$DB_NAME' AND pid <> pg_backend_pid();\""

# Drop the database
echo "Dropping database if it exists..."
oc exec "$POD_NAME" -- bash -c "psql -U $DB_USER -c \"DROP DATABASE IF EXISTS $DB_NAME;\""

# Create fresh database
echo "Creating fresh database..."
oc exec "$POD_NAME" -- bash -c "psql -U $DB_USER -c \"CREATE DATABASE $DB_NAME OWNER = $DB_OWNER;\""

# Copy the backup file to the pod
echo "Copying backup file to the pod..."
oc cp "$BACKUP_FILE" "$POD_NAME:/tmp/$FILENAME"

# Verify file was copied by listing it in the /tmp directory
echo "Verifying file exists in pod's /tmp directory:"
oc exec "$POD_NAME" -- ls -lh "/tmp/$FILENAME"

# Execute the restore command with --no-owner to make DB_OWNER the owner of all objects
echo "Restoring database from backup..."
oc exec "$POD_NAME" -- bash -c "PGCLIENTENCODING=WIN1252 pg_restore -U $DB_USER --no-owner -d $DB_NAME -v /tmp/$FILENAME"

# Set permissions one by one with simpler commands
echo "Setting permissions on restored objects..."

# Grant schema permissions
echo "Granting schema permissions..."
oc exec "$POD_NAME" -- bash -c "psql -U $DB_USER -d $DB_NAME -c \"GRANT ALL ON SCHEMA public TO $DB_OWNER;\""

# Grant permissions on all tables
echo "Granting table permissions..."
oc exec "$POD_NAME" -- bash -c "psql -U $DB_USER -d $DB_NAME -c \"GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO $DB_OWNER;\""

# Grant permissions on all sequences
echo "Granting sequence permissions..."
oc exec "$POD_NAME" -- bash -c "psql -U $DB_USER -d $DB_NAME -c \"GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO $DB_OWNER;\""

# Grant permissions on all functions
echo "Granting function permissions..."
oc exec "$POD_NAME" -- bash -c "psql -U $DB_USER -d $DB_NAME -c \"GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public TO $DB_OWNER;\""

# Alter default privileges for future objects
echo "Setting default privileges for future objects..."
oc exec "$POD_NAME" -- bash -c "psql -U $DB_USER -d $DB_NAME -c \"ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT ALL PRIVILEGES ON TABLES TO $DB_OWNER;\""
oc exec "$POD_NAME" -- bash -c "psql -U $DB_USER -d $DB_NAME -c \"ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT ALL PRIVILEGES ON SEQUENCES TO $DB_OWNER;\""
oc exec "$POD_NAME" -- bash -c "psql -U $DB_USER -d $DB_NAME -c \"ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT ALL PRIVILEGES ON FUNCTIONS TO $DB_OWNER;\""

echo "Database restore and permission setup completed."

# Clean up
echo "Cleaning up temporary files..."
oc exec "$POD_NAME" -- rm -f "/tmp/$FILENAME"

echo "Restore process completed."