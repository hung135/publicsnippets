import java.sql.*;
import java.util.*;
/**
javac --release 11 -cp "path/to/postgresql-driver.jar" -d bin src/DatabaseSchemaVisualizer.java
java --release 11 -cp "bin;path/to/postgresql-driver.jar" DatabaseSchemaVisualizer
**/
public class DatabaseSchemaVisualizer {
    private static final String DB_URL = "jdbc:postgresql://localhost/your_database_name";
    private static final String DB_USER = "your_username";
    private static final String DB_PASSWORD = "your_password";
    private static final String SCHEMA = "public";

    public static void main(String[] args) {
        try (Connection conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD)) {
            List<String> tables = fetchTables(conn, SCHEMA);
            Map<String, List<Column>> tableColumns = fetchTableColumns(conn, SCHEMA, tables);
            List<ForeignKeyRelationship> relationships = fetchRelationships(conn, SCHEMA);

            String mermaidDiagram = generateMermaidDiagram(tables, tableColumns, relationships);
            System.out.println(mermaidDiagram);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static List<String> fetchTables(Connection conn, String schema) throws SQLException {
        List<String> tables = new ArrayList<>();
        String query = "SELECT table_name FROM information_schema.tables WHERE table_schema = ?";
        try (PreparedStatement stmt = conn.prepareStatement(query)) {
            stmt.setString(1, schema);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                tables.add(rs.getString("table_name"));
            }
        }
        return tables;
    }

    private static Map<String, List<Column>> fetchTableColumns(Connection conn, String schema, List<String> tables) throws SQLException {
        Map<String, List<Column>> tableColumns = new HashMap<>();
        String query = "SELECT table_name, column_name, data_type FROM information_schema.columns WHERE table_schema = ? AND table_name = ?";
        for (String table : tables) {
            List<Column> columns = new ArrayList<>();
            try (PreparedStatement stmt = conn.prepareStatement(query)) {
                stmt.setString(1, schema);
                stmt.setString(2, table);
                ResultSet rs = stmt.executeQuery();
                while (rs.next()) {
                    Column column = new Column();
                    column.name = rs.getString("column_name");
                    column.dataType = rs.getString("data_type");
                    columns.add(column);
                }
            }
            tableColumns.put(table, columns);
        }
        return tableColumns;
    }

    private static List<ForeignKeyRelationship> fetchRelationships(Connection conn, String schema) throws SQLException {
        List<ForeignKeyRelationship> relationships = new ArrayList<>();
        String query = "SELECT tc.table_name, kcu.column_name, ccu.table_name AS foreign_table_name, ccu.column_name AS foreign_column_name " +
                       "FROM information_schema.table_constraints AS tc " +
                       "JOIN information_schema.key_column_usage AS kcu ON tc.constraint_name = kcu.constraint_name AND tc.table_schema = kcu.table_schema " +
                       "JOIN information_schema.constraint_column_usage AS ccu ON ccu.constraint_name = tc.constraint_name AND ccu.table_schema = tc.table_schema " +
                       "WHERE tc.constraint_type = 'FOREIGN KEY' AND tc.table_schema = ?";
        try (PreparedStatement stmt = conn.prepareStatement(query)) {
            stmt.setString(1, schema);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                ForeignKeyRelationship relationship = new ForeignKeyRelationship();
                relationship.table = rs.getString("table_name");
                relationship.column = rs.getString("column_name");
                relationship.foreignTable = rs.getString("foreign_table_name");
                relationship.foreignColumn = rs.getString("foreign_column_name");
                relationships.add(relationship);
            }
        }
        return relationships;
    }

    private static String generateMermaidDiagram(List<String> tables, Map<String, List<Column>> tableColumns, List<ForeignKeyRelationship> relationships) {
        StringBuilder diagram = new StringBuilder("erDiagram\n");

        for (String table : tables) {
            diagram.append(" ").append(table).append(" {\n");
            for (Column column : tableColumns.get(table)) {
                diagram.append("  ").append(column.dataType).append(" ").append(column.name).append("\n");
            }
            diagram.append(" }\n");
        }

        for (ForeignKeyRelationship relationship : relationships) {
            diagram.append(" ").append(relationship.table).append(" ||--||{ ").append(relationship.foreignTable).append(" : \"").append(relationship.column).append(" references ").append(relationship.foreignColumn).append("\"\n");
        }

        return diagram.toString();
    }

    private static class Column {
        String name;
        String dataType;
    }

    private static class ForeignKeyRelationship {
        String table;
        String column;
        String foreignTable;
        String foreignColumn;
    }
}