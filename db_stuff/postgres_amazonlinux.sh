#!/bin/bash

# Update the system and install dependencies
yum update -y
yum groupinstall -y "Development Tools"
yum install -y readline-devel zlib-devel openssl-devel

# Download PostgreSQL source code (version 15)
POSTGRES_VERSION="15.3"
wget https://ftp.postgresql.org/pub/source/v${POSTGRES_VERSION}/postgresql-${POSTGRES_VERSION}.tar.gz
tar xzf postgresql-${POSTGRES_VERSION}.tar.gz
cd postgresql-${POSTGRES_VERSION}

# Configure PostgreSQL
./configure --prefix=/usr/local/pgsql

# Compile and install
make
make install

# Create a PostgreSQL user
useradd postgres

# Create necessary directories and set permissions
mkdir /usr/local/pgsql/data
chown postgres:postgres /usr/local/pgsql/data

# Initialize the database
su - postgres -c "/usr/local/pgsql/bin/initdb -D /usr/local/pgsql/data"

# Optional: Start PostgreSQL (you may want to create a proper service instead)
# su - postgres -c "/usr/local/pgsql/bin/pg_ctl -D /usr/local/pgsql/data -l logfile start"

# Add PostgreSQL binaries to the system PATH
echo "export PATH=$PATH:/usr/local/pgsql/bin" >> ~/.bashrc
source ~/.bashrc