-- Create a table for storing address information
CREATE TABLE address (
    address_id SERIAL PRIMARY KEY,
    street VARCHAR(255),
    city VARCHAR(100),
    state VARCHAR(100),
    zip_code VARCHAR(20),
    country VARCHAR(100)
);

-- Create a table for storing employee information
CREATE TABLE employee (
    employee_id SERIAL PRIMARY KEY,
    first_name VARCHAR(100),
    last_name VARCHAR(100),
    email VARCHAR(255) UNIQUE,
    phone_number VARCHAR(20),
    address_id INT,
    CONSTRAINT fk_address
        FOREIGN KEY(address_id) 
        REFERENCES address(address_id)
        ON DELETE SET NULL
);

-- Create a table for storing timesheet entries
CREATE TABLE timesheet (
    timesheet_id SERIAL PRIMARY KEY,
    employee_id INT,
    work_date DATE,
    hours_worked DECIMAL(5,2),
    work_description TEXT,
    CONSTRAINT fk_employee
        FOREIGN KEY(employee_id)
        REFERENCES employee(employee_id)
        ON DELETE CASCADE
);

-- Optional: Create a table for storing roles or departments
CREATE TABLE department (
    department_id SERIAL PRIMARY KEY,
    department_name VARCHAR(255)
);

-- Optional: Link employees to departments (assuming an employee belongs to only one department)
ALTER TABLE employee
ADD COLUMN department_id INT,
ADD CONSTRAINT fk_department
    FOREIGN KEY(department_id)
    REFERENCES department(department_id)
    ON DELETE SET NULL;
